from plot_grid_map import plot_grid_map
import pandas as pd

df = pd.read_csv('data/grid_map_rus - example.csv')
features_list = ['Аграрный сектор',
                 'Добыча полезных ископаемых',
                 'Промышленность',
                 'Инфраструктура',
                 'Торговля и сервис',
                 'Услуги',
                 'Бюджетный сектор']


plot_grid_map(df, features_list,
              shuffle=True,
              pixels=20,
              bar_plot=False,
              tile_names='subj_rus',
              fed_distr_color=True)
